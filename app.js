// Code du jeux------------------------------------------------------

let nombreRandom = Math.floor(Math.random() * 99) + 1;
let essaisRestant = 10;
let valeurJoueur;
const btnSubmit = document.querySelector('#btnSubmit');
const proposition = document.querySelector('#proposition');
//function interdisant de coller du texte
window.onload = function () {
    const proposition = document.getElementById('proposition');
    proposition.onpaste = function (e) {
        e.preventDefault();
    }
}

//vérification de frappe
function verificationFrappe(event) {

    let keyCode = event.which ? event.which : event.keyCode;
    let touche = String.fromCharCode(keyCode);

    let champ = document.getElementById('proposition');

    let caracteres = '0123456789';

    if (caracteres.indexOf(touche) >= 0) {
        champ.value += touche;
    }
}

//vérification de valeurJoueur
btnSubmit.addEventListener('click', function (event) {
    valeurJoueur = document.querySelector('#proposition');
    if (valeurJoueur.value < 100 && valeurJoueur.value > 0 && valeurJoueur.value == parseInt(valeurJoueur.value, 10)) {
        comparaison();
    } else {
        document.getElementById('retour').innerHTML = "Doucement bonhomme, choisi un nombre entier entre 1 et 99";
    }
});

proposition.addEventListener('keypress', function enter (event) {
    valeurJoueur = document.querySelector('#proposition');
    if (event.keyCode == 13){
        if (valeurJoueur.value < 100 && valeurJoueur.value > 0 && valeurJoueur.value == parseInt(valeurJoueur.value, 10)) {
            comparaison();
        } else {
            document.getElementById('retour').innerHTML = "Doucement jeune Padawan, un nombre entier compris entre 1 et 99 tu dois choisir !";
            document.getElementById("proposition").focus();
            document.getElementById("proposition").value = "";
        }
    } else {
        verificationFrappe();
    }
    
});

// fonction comparaison des résultats

function comparaison() {
    if (valeurJoueur.value == nombreRandom) {
        swal({
            title: 'Bravo jeune padawan !',
            text: 'Maintenant la force tu maîtrises',
            confirmButtonText: 'Poursuivre ton entrainement',
            imageUrl: 'https://media.giphy.com/media/6fScAIQR0P0xW/giphy.gif',
            imageWidth: 400,
            imageHeight: 300,
            imageAlt: 'Custom image',
            backdrop: `rgba(76, 209, 55,0.4)`,
            animation: false,
            customClass: 'animated bounceIn'
        }).then(function () {
            location.reload();
        });
    } else {
        essaisRestant--;           
        createElement();     
        valeurData();
        indice();
        countDown();
        coupsRestant();
        changeYoda();
    }
};

//fonction indice
function indice() {
    console.log(valeurJoueur.value);
    if (valeurJoueur.value < nombreRandom) {
       
        let txt1 = "C'est ";
        let txt2 = ", essaie encore";
        
        document.getElementById('retour').innerHTML = txt1 + "<span class ='spanPlus'>plus</span>" + txt2;
        
    } else if (valeurJoueur.value > nombreRandom) {
       
        let txt1 = "C'est ";
        let txt2 = ", essaie encore";
        
        document.getElementById('retour').innerHTML = txt1 + "<span class ='spanMoins'>moins</span>" + txt2;
        console.log(valeurJoueur.value);
        console.log('moins');
    }
};

//fonction coups restants
function coupsRestant() {
    document.getElementById('coupsRestant').innerHTML = essaisRestant;
}

//fonction décompte d'essais
function countDown() {
    if (essaisRestant == 0) {
        swal({
            title: 'TES LEÇONS TU DOIS APPRENDRE!',
            text: 'Mauvais tu es, t"entrainer plus tu dois !',
            confirmButtonText: 'Poursuivre ton entrainement',
            imageUrl: 'https://media.giphy.com/media/qxkQ5PLLCnXW0/giphy.gif',
            imageWidth: 400,
            imageHeight: 300,
            imageAlt: 'Custom image',
            backdrop: `rgba(232, 65, 24,0.4)`,
            animation: false,
            customClass: 'animated shake',
        }).then(function () {
            location.reload();
        });
    } else {
        document.getElementById("proposition").focus();
        document.getElementById("proposition").value = "";
    }
};
// --------------------------------------------------------------------------------------
// fonction changement de yoda
function changeYoda(){
    let b = document.getElementById("imageYoda");

    if(essaisRestant <= 5 && essaisRestant > 1) {
        b.src = "./media/yoda_suspicious.png";
    }
    if (essaisRestant == 1) {

        b.src="./media/yoda_angry.png";
    }
}

// Données des joueurs dans la div "#dataPlayer"
function createElement() {
    let createParaElement = document.createElement("p");
    let node = document.createTextNode(valeurJoueur.value);
    createParaElement.appendChild(node);
    let element = document.getElementById("dataPlayer");
    element.appendChild(createParaElement);
    createParaElement.classList.add('classPara');
}

function valeurData() {
    if (essaisRestant < 10) {
        document.getElementById('valeurData').innerHTML = "Ces nombres tu as deja essaye :";
    }
};
